﻿using iControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace newF5
{
    public partial class Informational : Form
    {

        Exporter exporter;


        public Informational(string ip, string user, string pass)
        {
            InitializeComponent();

            Dloadbtn.Enabled = false;

            currTimeLbl.Text = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            label2.Text = interfacesSingleton.getInstance().ic.ConnectionInfo.hostname;
            label4.Text = interfacesSingleton.getInstance().ic.ConnectionInfo.username;
            rptPass.PasswordChar = '*';


        }

        private void stats_Load(object sender, EventArgs e)
        {
            comboBox2.Items.AddRange(interfacesSingleton.getInstance().ic
                .ManagementPartition.get_partition_list()
                .Select(x => x.partition_name).ToArray());
        }

        private void btnpar_virt_Click(object sender, EventArgs e)
        {

            string seltext = comboBox2.SelectedItem.ToString();
            interfacesSingleton.getInstance().ic.ManagementPartition.set_active_partition(seltext);
            if (comboBox2.SelectedIndex > 0)
            {
                vsTxtBox.Clear();
                interfacesSingleton.getInstance().ic.SystemSession.set_recursive_query_state(CommonEnabledState.STATE_ENABLED);
                List<string> servers = interfacesSingleton.getInstance().ic
                    .LocalLBVirtualServer.get_list().ToList();
                List<CommonAddressPort> CommonAddressPorts = interfacesSingleton.getInstance().ic.LocalLBVirtualServer.get_destination_v2(servers.ToArray()).ToList();

                List<string> results = CommonAddressPorts.Join(servers, x => CommonAddressPorts.IndexOf(x), y => servers.IndexOf(y), (x, y) => y + " = " + x.address.Split('/').Last().Split('%')[0] + ":" + x.port).ToList();
                vsTxtBox.AppendText(string.Join(Environment.NewLine, results));

            }
            else
            {
                MessageBox.Show("Please select an item from the list");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void reportBtn_Click(object sender, EventArgs e)
        {
            string User = rptUser.Text;
            string Pass = rptPass.Text;

            reportBtn.Enabled = false;
            btnreport2.Enabled = false;
            rptLoginPanel.Enabled = false;

            if (rptUser.Text.Length == 0 && rptPass.Text.Length == 0)
            {
                MessageBox.Show("Please input your credentials to access this report.");
            }
            else
            {

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();


                saveFileDialog1.FileName = "F5 Virtual Server Report";
                saveFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    exporter = new Exporter();
                    ListenToExporter(exporter);
                    Task.Factory.StartNew(() => exporter.ExportAllDevices(User, Pass, saveFileDialog1.FileName));

                }
            }

        }
        private void OnGettingLoadBalancer(object sender, ExporterEventArgs e)
        {
            pbarTotal.SetPropertyThreadSafe(() => pbarTotal.Value, pbarTotal.Value + 1);
            lblTotal.SetPropertyThreadSafe(() => lblTotal.Text, e.Message);
            pbarCurrent.SetPropertyThreadSafe(() => pbarCurrent.Value, 0);
            pbarCurrent.SetPropertyThreadSafe(() => pbarCurrent.Maximum, e.Count);
        }
        private void OnGettingPartition(object sender, ExporterEventArgs e)
        {
            pbarCurrent.SetPropertyThreadSafe(() => pbarCurrent.Value, pbarCurrent.Value + 1);
            lblCurrent.SetPropertyThreadSafe(() => lblCurrent.Text, e.Message);
        }
        private void OnExportCompleted(object sender, ExporterEventArgs e)
        {
            btnOK.SetPropertyThreadSafe(() => btnOK.Enabled, true);
            reportBtn.SetPropertyThreadSafe(() => reportBtn.Enabled, true);
            btnreport2.SetPropertyThreadSafe(() => btnreport2.Enabled, true);
            lblTotal.SetPropertyThreadSafe(() => lblTotal.Text, "Export Completed");
            lblCurrent.SetPropertyThreadSafe(() => lblCurrent.Text, "Click 'OK'");
        }
        private void OnFileOpenError(object sender, ErrorEventArgs e)
        {
            MessageBox.Show(e.GetException().Message, "There was an error saving the file");
        }
        private void ListenToExporter(Exporter exporter)
        {
            pnlProgress.Visible = true;
            pbarTotal.Maximum = Properties.Settings.Default.LoadBalancers.Count;
            exporter.GettingLoadBalancer += OnGettingLoadBalancer;
            exporter.GettingPartition += OnGettingPartition;
            exporter.ExportCompleted += OnExportCompleted;
            exporter.FileOpenError += OnFileOpenError;
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {


        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            pnlProgress.Visible = false;
            btnOK.Enabled = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void UCSbtn_Click(object sender, EventArgs e)
        {

            SystemConfigSyncSaveMode SAVE_FULL = default(SystemConfigSyncSaveMode);
            iControl.Interfaces ic = new Interfaces();
            interfacesSingleton.getInstance().ic.SystemConfigSync.save_configuration(label4.Text, SAVE_FULL);

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Seabtn_Click(object sender, EventArgs e)
        {


        }

        private void desearbtn_Click_1(object sender, EventArgs e)
        {
            DBView Se = new DBView(DBController.GetDBFullPath());

            Se.Show();
        }

        private void DelUcs_Click(object sender, EventArgs e)
        {

            string FileToUndo = UcsBox.GetItemText(UcsBox.SelectedItem);
           
            //MessageBox.Show(FileToUndo)

            if (!string.IsNullOrEmpty(FileToUndo))
            {

                interfacesSingleton.getInstance().ic.SystemConfigSync.delete_configuration(FileToUndo);
                MessageBox.Show("File " + FileToUndo + " has been deleted.");
                UcsBox.Items.RemoveAt(UcsBox.SelectedIndex);
                
            }
            else
            {

                MessageBox.Show("File does not exist!");

            }
            
        }

        private void ucsviewbtn_Click(object sender, EventArgs e)
        {

            SystemConfigSyncConfigFileEntry[] list = interfacesSingleton.getInstance().ic.SystemConfigSync.get_configuration_list();

            UcsBox.Items.Clear();
            for (int i = 0; i < list.Length; i++)
            {                
                UcsBox.Items.Add(list[i].file_name.ToString());
            }

        }
    }
}
