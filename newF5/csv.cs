﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

namespace newF5
{
    public static class CSVX
    {
        #region "CSV Conversion"
        private static List<List<string>> DataTableToList(DataTable dt)
        {
            List<List<string>> list = new List<List<string>>();
            list = (dt.AsEnumerable().Select(row => row.ItemArray.Select(val => val.ToString()).ToList())).ToList();
            return list;
        }
        private static string csvQuote(string value)
        {
            // Make this safe for export as CSV, ie. surrounded by quotes so no extra separater and escape any intended quotes
            // The extra quotes are needed to prevent excel from stripping leading zeros.  And even more quotes are needed to 
            // then prevent excel from splitting a field that has a comma in it.
            return String.Concat("\"=\"\"", value.Trim().Replace("\"", "\"\""), "\"\"\"");
        }
        public static string DataTableToCsv(DataTable dt)
        {
            string header = String.Join(",", dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray());
            List<string> data = new List<string>();
            data.Add(header);
            foreach (List<string> item in DataTableToList(dt))
            {
                string newitem = String.Join(",", item.Select(a => csvQuote(a.ToString())));
                data.Add(newitem);
            }
            return String.Join(Environment.NewLine, data);
        }
        #endregion

        public static string EnumerableToCSV<T>(IList<T> li, PropertyType pt = PropertyType.Named)
        {
            DataTable table = EnumerableToTable<T>(li, pt);
            return DataTableToCsv(table);
        }
        public static DataTable EnumerableToTable<T>(IList<T> li, PropertyType pt = PropertyType.Named)
        {
            DataTable table = EmptyTable<T>(pt);
            PopulateTableFromList<T>(table, ref li);
            return table;
        }
        public static void PopulateTableFromList<T>(DataTable table, ref IList<T> li, PropertyType pt = PropertyType.Named)
        {
            foreach (T obj in li)
            {
                List<PropertyInfo> propList = typeof(T).GetProperties().ToList();
                DataRow dr = table.NewRow();
                foreach (PropertyInfo prop in propList)
                {
                    //if we dont care about browsability or the property is browsable then continue
                    if ((pt != PropertyType.Browsable && pt != PropertyType.NamedBrowsable) || !(Attribute.IsDefined(prop, typeof(BrowsableAttribute)) && !prop.GetCustomAttributes(typeof(BrowsableAttribute), false).Cast<BrowsableAttribute>().Single().Browsable))
                    {
                        //make sure it is named
                        if (Attribute.IsDefined(prop, typeof(DisplayNameAttribute)))
                        {
                            if (table.Columns.Contains(prop.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast<DisplayNameAttribute>().Single().DisplayName))
                            {
                                dr.SetField(prop.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast<DisplayNameAttribute>().Single().DisplayName, prop.GetValue(obj));
                            }
                        }
                        //if not then make sure we dont care about the name
                        else if (pt != PropertyType.Named && pt != PropertyType.NamedBrowsable)
                        {
                            if (table.Columns.Contains(prop.Name))
                            {
                                dr.SetField(prop.Name, prop.GetValue(obj));
                            }
                        }
                    }


                }
                table.Rows.Add(dr);
            }
        }
        public static DataTable EmptyTable<T>(PropertyType pt = PropertyType.Named)
        {
            //Create DataTable from list object's properties
            DataTable table = new DataTable();
            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                //if we dont care about browsability or the property is browsable then continue
                if ((pt != PropertyType.Browsable && pt != PropertyType.NamedBrowsable) || !(Attribute.IsDefined(prop, typeof(BrowsableAttribute)) && !prop.GetCustomAttributes(typeof(BrowsableAttribute), false).Cast<BrowsableAttribute>().Single().Browsable))
                {
                    //make sure it is named
                    if (Attribute.IsDefined(prop, typeof(DisplayNameAttribute)))
                    {
                        if (!table.Columns.Contains(prop.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast<DisplayNameAttribute>().Single().DisplayName) )
                        {
                            if (!prop.PropertyType.Name.ToLower().Contains("null"))
                            {
                                table.Columns.Add(prop.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast<DisplayNameAttribute>().Single().DisplayName, prop.PropertyType);
                            }
                            else
                            {
                                table.Columns.Add(prop.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast<DisplayNameAttribute>().Single().DisplayName, prop.PropertyType.GenericTypeArguments[0]);
                            }
                        }
                    }
                    //if not then make sure we dont care about the name
                    else if (pt != PropertyType.Named && pt != PropertyType.NamedBrowsable )
                    {
                        if (!prop.PropertyType.Name.ToLower().Contains("null"))
                        {
                            table.Columns.Add(prop.Name, prop.PropertyType);
                        }
                        else
                        {
                            table.Columns.Add(prop.Name, prop.PropertyType.GenericTypeArguments[0]);
                        }

                    }
                }
            }
            return table;
        }

        
    }
    public enum PropertyType
    {
        ALL,
        Named,
        Browsable,
        NamedBrowsable
    }
}
