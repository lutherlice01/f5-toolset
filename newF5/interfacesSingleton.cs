﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newF5
{
    class interfacesSingleton
    {
        private iControl.Interfaces _ic;
        public iControl.Interfaces ic
        {
            get
            {
                        if (_ic == null)
                        {
                            _ic = new iControl.Interfaces();
                        }
                return _ic;
            }
            set
                        {
                            _ic = value;
                        }
        }
        private static interfacesSingleton instance;

        private  interfacesSingleton()
        {

        }

        public static interfacesSingleton getInstance()
        {
            if (instance == null)
            {
                instance = new interfacesSingleton();
            }

            return instance;
        }


    }
}
