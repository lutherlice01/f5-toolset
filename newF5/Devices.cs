﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newF5
{
    public class deviceOutputModel
    {
        [DisplayName("Load Balancer")]
        public string LoadBalancer { get; set; }
        [DisplayName("Partition Name")]
        public string Partition { get; set; }
        [DisplayName("Virtual Server")]
        public string Server { get; set; }
        [DisplayName("IP Address")]
        public string IP { get; set; }
        [DisplayName("Port")]
        public string Port { get; set; }

    }
    public class Partition
    {
        [DisplayName("Partition Name")]
        public string PartitionName { get; set; }
        public List<VirtualServer> virtualServer { get; set; }
    }
    public class VirtualServer
    {
        [DisplayName("Virtual Server Name")]
        public string Name { get; set; }
        public List<Socket> Sockets { get; set; }
        
    }    
    public class Socket
    {
        [DisplayName("Port")]
        public string Port { get; set; }        
        [DisplayName("IP Address")]
        public string IPAddress { get; set; }
    }
}
