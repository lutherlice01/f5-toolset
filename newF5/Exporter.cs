﻿using iControl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace newF5
{

    

    public  class Exporter
    {
        #region events 
        public event GettingLoadBalancerEventHandler GettingLoadBalancer;
        public event GettingPartitionEventHandler GettingPartition;
        public event ExportCompletedEventHandler ExportCompleted;
        public event FileOpenExceptionEventHandler FileOpenError;

        public void OnGettingLoadBalancer(object sender, ExporterEventArgs e)
        {
            GettingLoadBalancer?.Invoke(sender, e);
        }
        public void OnGettingPartition(object sender, ExporterEventArgs e)
        {
            GettingPartition?.Invoke(sender, e);
        }
        public void OnExportCompleted(object sender, ExporterEventArgs e)
        {
            ExportCompleted?.Invoke(sender, e);
        }
        public void OnFileOpenError(object sender, ErrorEventArgs e)
        {
            FileOpenError?.Invoke(sender, e);
        }
        #endregion

        public void ExportAllDevices(string user, string pass, string filepath)
        {
            iControl.Interfaces ic = new Interfaces();
            List<deviceOutputModel> output = new List<deviceOutputModel>();
            foreach (string loadBalancer in Properties.Settings.Default.LoadBalancers)
            {
                if (ic.initialize(loadBalancer, user, pass))
                {

                    int count = ic
                    .ManagementPartition.get_partition_list().Count();

                    OnGettingLoadBalancer(this, new ExporterEventArgs("Getting partitions from " + loadBalancer, count));

                    foreach (string partition in ic
                    .ManagementPartition.get_partition_list()
                    .Select(x => x.partition_name).ToList())
                    {
                        OnGettingPartition(this, new ExporterEventArgs("Getting virtual servers from " + partition));
                        ic.ManagementPartition.set_active_partition(partition);
                        ic.SystemSession.set_recursive_query_state(CommonEnabledState.STATE_ENABLED);
                        List<string> servers = ic.LocalLBVirtualServer.get_list().ToList();
                        List<CommonAddressPort> CommonAddressPorts = ic.LocalLBVirtualServer.get_destination_v2(servers.ToArray()).ToList();

                        List<deviceOutputModel> results = CommonAddressPorts
                            .Join(servers, x => CommonAddressPorts.IndexOf(x), y => servers.IndexOf(y),
                                                    (x, y) => new deviceOutputModel()
                                                    {
                                                        LoadBalancer = loadBalancer,
                                                        Partition = partition,
                                                        Server = y.Split('/').Last(),
                                                        IP = x.address.Split('/').Last().Split('%')[0],
                                                        Port = x.port.ToString()
                                                    }
                                                        ).ToList();
                        output.AddRange(results);
                    }

                    
                }
                else
                {
                    throw new Exception("Could not initialize");
                }
            }
            DBController.WriteDB(output);
            string csvString = CSVX.EnumerableToCSV(output);
            try
            {
                File.WriteAllText(filepath, csvString);
            }
            catch (IOException ex)
            {
                OnFileOpenError(this, new ErrorEventArgs(ex));
            }
            OnExportCompleted(this, new newF5.ExporterEventArgs("Completed"));
        }

        private void WriteFile()
        {
            throw new NotImplementedException();
        }
    }
    public delegate void GettingLoadBalancerEventHandler(object sender, ExporterEventArgs e);
    public delegate void GettingPartitionEventHandler(object sender, ExporterEventArgs e);
    public delegate void ExportCompletedEventHandler(object sender, ExporterEventArgs e);
    public delegate void FileOpenExceptionEventHandler(object sender, ErrorEventArgs e);
    public class ExporterEventArgs : EventArgs
    {
        public ExporterEventArgs(string msg, int cnt = 0)
        {
            Message = msg;
            Count = cnt;
        }
        public string Message { get; private set; }
        public int Count { get; private set; }
    }
}
