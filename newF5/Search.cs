﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SearcherLib;

namespace newF5
{
    public partial class Search<T> : Form
        where T : class, new()
    {
        public SearcherBuilder<T> builder = new SearcherBuilder<T>();
        List<Filter> filters = new List<Filter>();
        public Search()
        {
            InitializeComponent();
        }
        private void Search_Load(object sender, EventArgs e)
        {
            LoadGrid();
            ddlProperty.DataSource = typeof(T).GetProperties().Select(x => x.Name).ToList();
            ddlSearchType.DataSource = Enum.GetNames(typeof(StringSearchTypes));
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            SearchType st;
            SearchType.TryParse(ddlSearchType.Text, out st);
            filters.Add(new Filter(ddlProperty.Text, txtValue.Text, st, Comparison.And, false, cbxNOT.Checked));
            LoadGrid();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            foreach( Filter f in filters)
            {
                builder.AddFilter(f);
            }
            this.DialogResult = DialogResult.OK;
        }

        private void grdSearch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                filters.RemoveAt(e.RowIndex);
            }
            LoadGrid();
        }
        private void LoadGrid()
        {
            grdSearch.DataSource = null;
            grdSearch.DataSource = filters;
            grdSearch.Columns["FieldName"].DisplayIndex = 1;
            grdSearch.Columns["NOT"].DisplayIndex = 2;
            grdSearch.Columns["TypeOfSearch"].DisplayIndex = 3;
            grdSearch.Columns["FieldValue"].DisplayIndex = 4;
            grdSearch.Columns["MatchCase"].Visible = false;
        }
    }
}
