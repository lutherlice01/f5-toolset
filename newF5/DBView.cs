﻿using SearcherLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace newF5
{
    public partial class DBView : Form
    {
        List<deviceOutputModel> gridData = new List<deviceOutputModel>();
        List<deviceOutputModel> filterData = new List<deviceOutputModel>();
        public DBView(string file)
        {
            InitializeComponent();
            gridData = 
                (List<deviceOutputModel>)new XmlSerializer(typeof(List<deviceOutputModel>))
                .Deserialize(File.Open(file,FileMode.Open));
        }

        public DBView()
        {
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search<deviceOutputModel> sForm = new Search<deviceOutputModel>();
            if (sForm.ShowDialog() == DialogResult.OK)
            {
                SearcherBuilder<deviceOutputModel> b = sForm.builder;
                b.SetSource(gridData.AsQueryable());
                filterData = b.FilterNatural().ToList();
                grdData.DataSource = null;
                grdData.DataSource = filterData;
            }            
        }

        private void DBView_Load(object sender, EventArgs e)
        {
            grdData.DataSource = gridData;            
        }

        private void btnClearSearch_Click(object sender, EventArgs e)
        {
            grdData.DataSource = null;
            grdData.DataSource = gridData;
        }

        private void DBView_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        internal static void show()
        {
            throw new NotImplementedException();
        }
    }
}
