﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iControl;

namespace newF5
{
    public partial class Form1 : Form
    {
     

        public Form1()
        {
            InitializeComponent();
            passtxt.PasswordChar = '*';
           // passtxt.PasswordChar = '*';

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (string loadBalancer in Properties.Settings.Default.LoadBalancers)
            {
                comboBox1.Items.Add(loadBalancer);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ip = comboBox1.Text;
            string user = usertxt.Text;
            string pass = passtxt.Text;
            
            if (interfacesSingleton.getInstance().ic.initialize(ip, user, pass))
            {
                var stats = new Informational(ip, user, pass);
                Hide();
                stats.ShowDialog();
                Show();
            }
        }

        private void lblCloseMain_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    }

