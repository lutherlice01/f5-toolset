﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace newF5
{
    public class DBController
    {
        public static void WriteDB(List<deviceOutputModel> output)
        {
            using (FileStream writer =
                            File.Create(Path.Combine(toolSet.CreateFolder(),
                            "DB" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".newf5")))
            {
                new XmlSerializer(typeof(List<deviceOutputModel>)).Serialize(writer, output);
            }
        }
        public static string GetDBFullPath()
        {
            return new DirectoryInfo(toolSet.CreateFolder()).GetFiles().OrderBy(x => x.CreationTime).Last().FullName;
        }
    }
}
