﻿namespace newF5
{
    partial class Informational
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Informational));
            this.vsTxtBox = new System.Windows.Forms.RichTextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.btnpar_virt = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Dloadbtn = new System.Windows.Forms.Button();
            this.currTimeLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.DelUcs = new System.Windows.Forms.Button();
            this.UcsBox = new System.Windows.Forms.ListBox();
            this.desearchlbl = new System.Windows.Forms.Label();
            this.desearbtn = new System.Windows.Forms.Button();
            this.ucsviewbtn = new System.Windows.Forms.Button();
            this.UCSbtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.rptLoginPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rptUser = new System.Windows.Forms.TextBox();
            this.rptPass = new System.Windows.Forms.TextBox();
            this.pnlProgress = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblCurrent = new System.Windows.Forms.Label();
            this.pbarCurrent = new System.Windows.Forms.ProgressBar();
            this.lblTotal = new System.Windows.Forms.Label();
            this.pbarTotal = new System.Windows.Forms.ProgressBar();
            this.btnreport2 = new System.Windows.Forms.Button();
            this.reportBtn = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ucsTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.rptLoginPanel.SuspendLayout();
            this.pnlProgress.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // vsTxtBox
            // 
            this.vsTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vsTxtBox.Location = new System.Drawing.Point(20, 60);
            this.vsTxtBox.Name = "vsTxtBox";
            this.vsTxtBox.Size = new System.Drawing.Size(733, 116);
            this.vsTxtBox.TabIndex = 9;
            this.vsTxtBox.Text = "";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(20, 21);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(215, 21);
            this.comboBox2.TabIndex = 10;
            // 
            // btnpar_virt
            // 
            this.btnpar_virt.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnpar_virt.Location = new System.Drawing.Point(269, 19);
            this.btnpar_virt.Name = "btnpar_virt";
            this.btnpar_virt.Size = new System.Drawing.Size(252, 23);
            this.btnpar_virt.TabIndex = 11;
            this.btnpar_virt.Text = "Get Virtual Server List";
            this.btnpar_virt.UseVisualStyleBackColor = true;
            this.btnpar_virt.Click += new System.EventHandler(this.btnpar_virt_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.Dloadbtn);
            this.groupBox1.Controls.Add(this.btnpar_virt);
            this.groupBox1.Controls.Add(this.vsTxtBox);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 194);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Partition List";
            // 
            // Dloadbtn
            // 
            this.Dloadbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dloadbtn.Location = new System.Drawing.Point(562, 19);
            this.Dloadbtn.Name = "Dloadbtn";
            this.Dloadbtn.Size = new System.Drawing.Size(179, 23);
            this.Dloadbtn.TabIndex = 12;
            this.Dloadbtn.Text = "Download";
            this.Dloadbtn.UseVisualStyleBackColor = true;
            // 
            // currTimeLbl
            // 
            this.currTimeLbl.AutoSize = true;
            this.currTimeLbl.Location = new System.Drawing.Point(11, 18);
            this.currTimeLbl.Name = "currTimeLbl";
            this.currTimeLbl.Size = new System.Drawing.Size(74, 13);
            this.currTimeLbl.TabIndex = 13;
            this.currTimeLbl.Text = "currentTime";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(179, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Device";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(644, 299);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(104, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 591);
            this.tabControl1.TabIndex = 18;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.DelUcs);
            this.tabPage2.Controls.Add(this.UcsBox);
            this.tabPage2.Controls.Add(this.desearchlbl);
            this.tabPage2.Controls.Add(this.desearbtn);
            this.tabPage2.Controls.Add(this.ucsviewbtn);
            this.tabPage2.Controls.Add(this.UCSbtn);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.btnClose);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.currTimeLbl);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(5);
            this.tabPage2.Size = new System.Drawing.Size(792, 565);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Virtual Server Info.";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // DelUcs
            // 
            this.DelUcs.Location = new System.Drawing.Point(307, 358);
            this.DelUcs.Name = "DelUcs";
            this.DelUcs.Size = new System.Drawing.Size(141, 30);
            this.DelUcs.TabIndex = 23;
            this.DelUcs.Text = "Delete UCS File";
            this.DelUcs.UseVisualStyleBackColor = true;
            this.DelUcs.Click += new System.EventHandler(this.DelUcs_Click);
            // 
            // UcsBox
            // 
            this.UcsBox.FormattingEnabled = true;
            this.UcsBox.Location = new System.Drawing.Point(14, 394);
            this.UcsBox.Name = "UcsBox";
            this.UcsBox.Size = new System.Drawing.Size(332, 56);
            this.UcsBox.TabIndex = 22;
            // 
            // desearchlbl
            // 
            this.desearchlbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.desearchlbl.AutoSize = true;
            this.desearchlbl.Location = new System.Drawing.Point(14, 299);
            this.desearchlbl.Name = "desearchlbl";
            this.desearchlbl.Size = new System.Drawing.Size(149, 13);
            this.desearchlbl.TabIndex = 21;
            this.desearchlbl.Text = "Search for an F5 object :";
            // 
            // desearbtn
            // 
            this.desearbtn.Location = new System.Drawing.Point(162, 294);
            this.desearbtn.Name = "desearbtn";
            this.desearbtn.Size = new System.Drawing.Size(104, 23);
            this.desearbtn.TabIndex = 20;
            this.desearbtn.Text = "Search";
            this.desearbtn.UseVisualStyleBackColor = true;
            this.desearbtn.Click += new System.EventHandler(this.desearbtn_Click_1);
            // 
            // ucsviewbtn
            // 
            this.ucsviewbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ucsviewbtn.Location = new System.Drawing.Point(14, 358);
            this.ucsviewbtn.Name = "ucsviewbtn";
            this.ucsviewbtn.Size = new System.Drawing.Size(138, 30);
            this.ucsviewbtn.TabIndex = 19;
            this.ucsviewbtn.Text = "View UCS Files";
            this.ucsviewbtn.UseVisualStyleBackColor = true;
            this.ucsviewbtn.Click += new System.EventHandler(this.ucsviewbtn_Click);
            // 
            // UCSbtn
            // 
            this.UCSbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UCSbtn.Location = new System.Drawing.Point(162, 358);
            this.UCSbtn.Name = "UCSbtn";
            this.UCSbtn.Size = new System.Drawing.Size(138, 30);
            this.UCSbtn.TabIndex = 18;
            this.UCSbtn.Text = "Create UCS File";
            this.ucsTip.SetToolTip(this.UCSbtn, "The UCS file created will overwrite \r\nthe default admin.ucs file.");
            this.UCSbtn.UseVisualStyleBackColor = true;
            this.UCSbtn.Click += new System.EventHandler(this.UCSbtn_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(409, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "currUser";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(792, 565);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Tools";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.domainUpDown1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(652, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(549, 19);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 6;
            this.button7.Text = "Email";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(433, 57);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 37);
            this.button6.TabIndex = 5;
            this.button6.Text = "Open in WireShark";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(433, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Download";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(318, 57);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Stop";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Location = new System.Drawing.Point(113, 28);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.ReadOnly = true;
            this.domainUpDown1.Size = new System.Drawing.Size(120, 20);
            this.domainUpDown1.TabIndex = 2;
            this.domainUpDown1.Text = "domainUpDown1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select the host :";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(318, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "Start";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 565);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Statistics";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.rptLoginPanel);
            this.tabPage4.Controls.Add(this.pnlProgress);
            this.tabPage4.Controls.Add(this.btnreport2);
            this.tabPage4.Controls.Add(this.reportBtn);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(792, 565);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Reporting";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // rptLoginPanel
            // 
            this.rptLoginPanel.Controls.Add(this.label5);
            this.rptLoginPanel.Controls.Add(this.label3);
            this.rptLoginPanel.Controls.Add(this.rptUser);
            this.rptLoginPanel.Controls.Add(this.rptPass);
            this.rptLoginPanel.Location = new System.Drawing.Point(12, 0);
            this.rptLoginPanel.Name = "rptLoginPanel";
            this.rptLoginPanel.Size = new System.Drawing.Size(192, 85);
            this.rptLoginPanel.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "UserName:";
            // 
            // rptUser
            // 
            this.rptUser.Location = new System.Drawing.Point(74, 17);
            this.rptUser.Name = "rptUser";
            this.rptUser.Size = new System.Drawing.Size(100, 20);
            this.rptUser.TabIndex = 23;
            // 
            // rptPass
            // 
            this.rptPass.Location = new System.Drawing.Point(74, 43);
            this.rptPass.Name = "rptPass";
            this.rptPass.Size = new System.Drawing.Size(100, 20);
            this.rptPass.TabIndex = 24;
            // 
            // pnlProgress
            // 
            this.pnlProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProgress.Controls.Add(this.btnOK);
            this.pnlProgress.Controls.Add(this.lblCurrent);
            this.pnlProgress.Controls.Add(this.pbarCurrent);
            this.pnlProgress.Controls.Add(this.lblTotal);
            this.pnlProgress.Controls.Add(this.pbarTotal);
            this.pnlProgress.Location = new System.Drawing.Point(8, 88);
            this.pnlProgress.Name = "pnlProgress";
            this.pnlProgress.Size = new System.Drawing.Size(792, 184);
            this.pnlProgress.TabIndex = 22;
            this.pnlProgress.Visible = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Enabled = false;
            this.btnOK.Location = new System.Drawing.Point(698, 141);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 20;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCurrent
            // 
            this.lblCurrent.AutoSize = true;
            this.lblCurrent.Location = new System.Drawing.Point(25, 85);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(52, 13);
            this.lblCurrent.TabIndex = 22;
            this.lblCurrent.Text = "Current:";
            // 
            // pbarCurrent
            // 
            this.pbarCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbarCurrent.Location = new System.Drawing.Point(19, 59);
            this.pbarCurrent.Name = "pbarCurrent";
            this.pbarCurrent.Size = new System.Drawing.Size(770, 23);
            this.pbarCurrent.TabIndex = 21;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(25, 43);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(40, 13);
            this.lblTotal.TabIndex = 20;
            this.lblTotal.Text = "Total:";
            // 
            // pbarTotal
            // 
            this.pbarTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbarTotal.Location = new System.Drawing.Point(19, 17);
            this.pbarTotal.Name = "pbarTotal";
            this.pbarTotal.Size = new System.Drawing.Size(770, 23);
            this.pbarTotal.TabIndex = 0;
            // 
            // btnreport2
            // 
            this.btnreport2.Location = new System.Drawing.Point(469, 17);
            this.btnreport2.Name = "btnreport2";
            this.btnreport2.Size = new System.Drawing.Size(191, 45);
            this.btnreport2.TabIndex = 21;
            this.btnreport2.Text = "Download VDC/EDC Report";
            this.btnreport2.UseVisualStyleBackColor = true;
            // 
            // reportBtn
            // 
            this.reportBtn.Location = new System.Drawing.Point(210, 17);
            this.reportBtn.Name = "reportBtn";
            this.reportBtn.Size = new System.Drawing.Size(191, 45);
            this.reportBtn.TabIndex = 20;
            this.reportBtn.Text = "Download Corporate Report";
            this.reportBtn.UseVisualStyleBackColor = true;
            this.reportBtn.Click += new System.EventHandler(this.reportBtn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.exitToolStripMenuItem.Text = "Exit Application";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Informational
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 615);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Informational";
            this.Text = "Informational";
            this.Load += new System.EventHandler(this.stats_Load);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.rptLoginPanel.ResumeLayout(false);
            this.rptLoginPanel.PerformLayout();
            this.pnlProgress.ResumeLayout(false);
            this.pnlProgress.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox vsTxtBox;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button btnpar_virt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label currTimeLbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button Dloadbtn;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel pnlProgress;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblCurrent;
        private System.Windows.Forms.ProgressBar pbarCurrent;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.ProgressBar pbarTotal;
        private System.Windows.Forms.Button btnreport2;
        private System.Windows.Forms.Button reportBtn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button UCSbtn;
        private System.Windows.Forms.Button ucsviewbtn;
        private System.Windows.Forms.ToolTip ucsTip;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox rptPass;
        private System.Windows.Forms.TextBox rptUser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel rptLoginPanel;
        private System.Windows.Forms.Label desearchlbl;
        private System.Windows.Forms.Button desearbtn;
        private System.Windows.Forms.ListBox UcsBox;
        private System.Windows.Forms.Button DelUcs;
    }
}